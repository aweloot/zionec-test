<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

header("Content-type: application/json");

try{
    require_once "classes/Router.php";
    require_once "classes/Api.php";
    $router = new Router();
    $curMethod = $router->getCurMethod();
    $api = new Api();

    if($curMethod["data"]){
        $response = $api->{$curMethod["method"]}($curMethod["data"]);
    } else {
        $response = $api->{$curMethod["method"]}();
    }

    die(json_encode($response));

} catch (Exception $e){
    die(json_encode(["success" => false, "message" => $e->getMessage()]));
}