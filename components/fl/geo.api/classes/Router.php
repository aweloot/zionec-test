<?php


class Router
{
    private const METHODS = [
        "add" => ["method" => "add", "fields" => "#fields#"],
        "update" => ["method" => "update", "id" => "#id#", "fields" => "#fields#"],
        "delete" => ["method" => "delete", "id" => "#id#"],
        "list" => ["method" => "getList"],
    ];

    /**
     * Router constructor.
     */
    function __construct()
    {

    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    function getCurMethod()
    {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $method["method"] = self::METHODS[$request->get("method")]["method"];
        $data = self::METHODS[$request->get("method")];
        unset($data["method"]);
        if ($method["method"]) {
            foreach ($data as $key => $value) {
                if (preg_match("/#(.+)#/", $value, $matches)) {
                    $data[$key] = $request->get($matches[1]);
                    if (!$data[$key]) {
                        throw new \Bitrix\Main\SystemException("Отсутствует параметр `$key`");
                    }

                }
            }
            $method["data"] = $data;
            return $method;
        } else {
            throw new \Bitrix\Main\SystemException("Unknown api method");
        }
    }

}