<?php


class Api
{
    private const HBLOCK_ID_GEO = 4;

    /**
     * Api constructor.
     */
    function __construct()
    {
        \Bitrix\Main\Loader::includeModule('highloadblock');
    }

    /**
     * @param array $arFields
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    function add(array $arFields){
        $hlBlock = $this->getHLBlock(self::HBLOCK_ID_GEO);
        $arHlFields = [
            "UF_NAME" => $arFields["fields"]["name"],
            "UF_ADDRESS" => $arFields["fields"]["address"],
            "UF_UPDATED_AT" => $arFields["fields"]["updated_at"] ?? ConvertTimeStamp(time(), "FULL"),
            "UF_CREATED_AT" => $arFields["fields"]["created_at"] ?? ConvertTimeStamp(time(), "FULL"),
        ];

        $res = $hlBlock::add($arHlFields);
        if($res->isSuccess()){
            return ["success" => true];
        } else {
            return ["success" => false, "message" => $res->getErrorMessages()];
        }
    }

    /**
     * @param array $arFields
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    function update(array $arFields){
        $hlBlock = $this->getHLBlock(self::HBLOCK_ID_GEO);
        $arHlFields = [
            "UF_NAME" => $arFields["fields"]["name"],
            "UF_ADDRESS" => $arFields["fields"]["address"],
            "UF_UPDATED_AT" => $arFields["fields"]["updated_at"] ?? ConvertTimeStamp(time(), "FULL"),
        ];

        $res = $hlBlock::update($arFields["id"], $arHlFields);
        if($res->isSuccess()){
            return ["success" => true];
        } else {
            return ["success" => false, "message" => $res->getErrorMessages()];
        }
    }

    /**
     * @param array $arFields
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    function delete(array $arFields){
        $hlBlock = $this->getHLBlock(self::HBLOCK_ID_GEO);
        $res = $hlBlock::delete($arFields["id"]);
        if($res->isSuccess()){
            return ["success" => true];
        } else {
            return ["success" => false, "message" => $res->getErrorMessages()];
        }
    }

    function getList(){
        $arGeoData = $this->getHLBlockData(self::HBLOCK_ID_GEO, [
            "select" => ["*"],
            "order" => ["ID" => "ASC"]
        ]);
        foreach ($arGeoData as $item) {
            $arResult[] = [
                "id" => $item["ID"],
                "name" => $item["UF_NAME"],
                "address" => $item["UF_ADDRESS"],
                "updated_at" => $item["UF_UPDATED_AT"]->format("Y-m-d H:i:s"),
                "created_at" => $item["UF_CREATED_AT"]->format("Y-m-d H:i:s"),
            ];
        }
        return json_encode($arResult);
    }

    /**
     * @param $HlBlockId
     * @return \Bitrix\Main\Entity\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    private function getHLBlock($HlBlockId)
    {
        if (empty($HlBlockId) || $HlBlockId < 1)
            throw new \Bitrix\Main\SystemException("");
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();
        return $entityDataClass;
    }

    /**
     * @param $hlBlockId
     * @param array $arFields
     * @param bool $key
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    function getHLBlockData($hlBlockId,array $arFields, $key = false)
    {
        $entityDataClass = $this->getHLBlock($hlBlockId);
        $dbRes = $entityDataClass::getList($arFields);
        if ($dbRes) {
            if ($key) {
                while ($arRes = $dbRes->fetch()) {
                    if ($arRes[$key]) {
                        $result[$arRes[$key]] = $arRes;
                    } else {
                        return false;
                    }
                }
                return $result;
            } else {
                return $dbRes->fetchAll();
            }
        } else {
            return false;
        }
    }

}